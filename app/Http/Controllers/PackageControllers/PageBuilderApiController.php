<?php

namespace App\Http\Controllers\PackageControllers;

use App\Http\Controllers\BaseControllers\BaseApiController;
use App\Http\Controllers\Controller;
use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChPagebuilder\Repositories\PageBuilderRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * @group PageBuilder
 *
 * APIs for PageBuilder
 */
class PageBuilderApiController extends BaseApiController
{

    /**
     * @var ExternalApiResponse
     */
    protected $response;
    /**
     * @var PageBuilderRepository
     */
    protected $pageBuilderRepository;


    public function getSearchableColumns()
    {
        return ['page_name'];
    }

    public function getOrderableColumns()
    {
        return ["pagebuilder.id"=>"id","pagebuilder.page_identifier"=>"identifier","pagebuilder.page_name"=>"name"];
    }

    public function getRepository()
    {
        return app(PageBuilderRepository::class);
    }

    public function getDefaultRelations(){
        return [];
    }

    /**
     * List all page structures
     *
     * pass Content-Language in the header
     *
     * @responseFile responses/pagebuilder-all.200.json
     * @param  Request  $request
     * @return void
     */
    public function index(Request $request)
    {
        try {
            $results = $this->getRepository()->all();
        } catch (\Exception $e) {
            return $this->response->failed($e->getMessage(), 500);
        }

        return $this->response->success($this->resource::collection($results));
    }


    /**
     * Display the specified page structure by id.
     *
     * pass Content-Language in the header
     * @urlParam id integer required The ID of the page structure.
     *
     * @responseFile responses/pagebuilder-single.200.json
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }


   /**
     * Get single page structure by filters
     *
     *
     * @queryParam where[0][column]
     * If u are using multiple where query params, it will support to AND operator in mysql
     * Possible values (id,page_identifier,page_name)
     *
     * @queryParam where[0][operator] Possible operators (=,>,<,>=,<=,like) if u are using like make sure to add % properly in the value as it is mysql operators. Example: like
     * @queryParam where[0][value] Value you are looking for. Example: %Lorem%
     *
     *
     * Ex api/v1/plugins/get-filtered-pagebuilder?where[0][operator]==&where[0][column]=page_identifier&where[0][value]=2022-10-12_PAGE_1665574050
     *
     * @responseFile responses/pagebuilder-single.200.json
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allWithAdvancedFilters(Request $request)
    {
        try {
            $items = $this->getFilteredItems($request,true);
            return $this->response->success(new $this->resource($items));
        }catch (ModelNotFoundException $exception) {
            return $this->response->failed('No records found', 404, $exception->getMessage(), $exception->getCode());
        }catch (\Exception $e) {
            return $this->response->failed($e->getMessage(), 500, $e->getTraceAsString());
        }
    }
}
