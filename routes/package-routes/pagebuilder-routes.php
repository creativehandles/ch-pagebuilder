<?php

Route::group(['name' => 'Pagebuilder', 'groupName' => "Page builder"], function () {
    Route::resource('pagebuilder', '\Creativehandles\ChPagebuilder\Http\Controllers\PluginsControllers\PageBuilderController');
    Route::get('pagebuilder-list','\Creativehandles\ChPagebuilder\Http\Controllers\PluginsControllers\PageBuilderController@pageBuilderGrid')->name('pageBuilderGrid');
});
