<?php

use Illuminate\Support\Facades\Route;

Route::group(['name' => 'page-builder-api', 'groupName' => 'page-builder-api','prefix'=>'plugins'], function () {
    Route::resource('/pagebuilder', 'PackageControllers\PageBuilderApiController')->only(['index', 'show']);
    Route::get('/get-filtered-pagebuilder', 'PackageControllers\PageBuilderApiController@allWithAdvancedFilters');
});
