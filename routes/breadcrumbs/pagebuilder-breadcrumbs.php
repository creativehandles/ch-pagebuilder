<?php

Breadcrumbs::for('admin.pagebuilder.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('ch-pagebuilder::pagebuilder.admin.pagebuilder.index'), route('admin.pagebuilder.index'));
});

Breadcrumbs::for('admin.pagebuilder.edit', function ($trail,$model) {
    $trail->parent('admin.pagebuilder.index');
    $trail->push(__('ch-pagebuilder::pagebuilder.admin.pagebuilder.edit'), route('admin.pagebuilder.edit',['pagebuilder'=>1]));
});

Breadcrumbs::for('admin.pagebuilder.create', function ($trail) {
    $trail->parent('admin.pagebuilder.index');
    $trail->push(__('ch-pagebuilder::pagebuilder.admin.pagebuilder.create'), route('admin.pagebuilder.create'));
});