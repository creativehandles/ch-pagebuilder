@extends('Admin.layout')
<?php
// app()->setLocale(LaravelLocalization::getCurrentLocale());
?>
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs', [
                        'header' => __('ch-pagebuilder::pagebuilder.Pagebuilder'),
                    ])
                </div>
            </div>
        </div>


    </div>
    <div class="row match-height">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="card-body">
                            <form action="" id="pageForm">

                                <div class="row">

                                    <div class="col-md-12">
                                        <h5>
                                            {{ __('ch-pagebuilder::pagebuilder.help.heading') }}:
                                        </h5>
                                        <ul>
                                            <li>{{ __('ch-pagebuilder::pagebuilder.help.step1') }} </li>
                                            <li>{{ __('ch-pagebuilder::pagebuilder.help.step2') }} </li>
                                            <li>{{ __('ch-pagebuilder::pagebuilder.help.step3') }} </li>
                                            <li>{{ __('ch-pagebuilder::pagebuilder.help.step4') }} </li>
                                            <li>{{ __('ch-pagebuilder::pagebuilder.help.step5') }} </li>
                                        </ul>

                                    </div>
                                    <hr>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('ch-pagebuilder::pagebuilder.pagename') }}</label>
                                            <input type="hidden" name="page_identifier"
                                                value="{{ isset($model) ? $model->page_identifier : join('_', [\Carbon\Carbon::now()->toDateString(), 'PAGE', \Carbon\Carbon::now()->timestamp]) }}">

                                            <input required type="text" class="form-control" id="pagename"
                                                placeholder="{{ __('ch-pagebuilder::pagebuilder.pagename') }}"
                                                name="pagename" value="{{ isset($model) ? $model->page_name : '' }}">
                                        </div>

                                        @if (isset($translate) && $translate)
                                            <div class="form-group">
                                                <label>{{ __('ch-pagebuilder::pagebuilder.language') }}</label>
                                                <select type="text" class="form-control" id="language" name="language">
                                                    @foreach ($availableLangs as $lang)
                                                        <option {{ $lang == app()->getLocale() ? 'selected' : '' }}
                                                            value="{{ $lang }}">{{ $lang }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        @endif

                                        @if (isset($requestedLang) && $requestedLang)
                                            <div class="form-group">
                                                <label>{{ __('ch-pagebuilder::pagebuilder.language') }}</label>
                                                <input type="text" readonly class="form-control" id="language"
                                                    name="language" value="{{ $requestedLang }}">
                                                </input>

                                            </div>
                                        @endif

                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12" id="galleryContainer">
                                                <div class="gallery" id="gallery-selector">
                                                </div>
                                                {{--                                        <input type="text" class="form-control" name="Pagename" > --}}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 sections">

                                            </div>
                                        </div>

                                        <div class="row">
                                            @if (in_array(
                                                \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                                                config('ch-pagebuilder.allowedRoles', ['Super Admin', 'Admin'])))
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-info" id="addSection">
                                                        <i class="fa fa-check-square-o"></i>
                                                        {{ __('ch-pagebuilder::pagebuilder.createSection') }}
                                                    </button>
                                                </div>
                                            @endif
                                            <a href="{{ URL::previous() }}">
                                                <button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{ __('trainings.general.goBack') }}
                                                </button></a>

                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-primary" id="generateForm">
                                                    <i class="fa fa-check-square-o"></i>
                                                    {{ __('ch-pagebuilder::pagebuilder.geneateform') }}
                                                </button>
                                            </div>
                                        </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">
                        {{ __('ch-pagebuilder::pagebuilder.galleryModal.heading') }}</h4>
                </div>
                <div class="modal-body">
                    <i class="fa fa-spinner fa-pulse"></i>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-primary"
                        data-dismiss="modal">{{ __('ch-pagebuilder::pagebuilder.galleryModal.confirmButton') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <style>
        .gallery_image {
            object-fit: cover;
            height: 150px;
            width: 150px;
            margin-left: 15px;
        }

        .image_picker_image {
            object-fit: cover;
            height: 150px;
            width: 150px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/ui/dragula.min.css') }}">
    <script src="{{ asset('vendors/js/extensions/dragula.min.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7"></script>

    <script>
        $(document).ready(function() {
            let nestedDrake = null;
            let appendGallery = true;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            @if (isset($model))

           <?php
            if(!$model->sections){
                    $locales = $model->getTranslatedLocales('sections');
                    $model->setLocale($locales[0]);
            }
            ?>
                @if ($model)
                    @foreach ($model->sections as $key => $section)
                        var result = {};
                        result.value = "{{ $section }}";
                        var newId = "section-{{ $section }}";
                        returnBlock(result, newId);

                        @foreach (\Illuminate\Support\Arr::get($model->content, $section, []) as $index => $item)


                            var fieldType = "{{ $item['type'] }}";

                            @if (is_array($item['value']))
                                var value = {!! json_encode(array_values(array_map('utf8_encode', $item['value']))) !!};
                            @else
                                var value = `{{ $item['value'] }}`;
                            @endif

                            var resultObject = {!! json_encode($item) !!};

                            var helpText = "{{ isset($item['helptext']) ? $item['helptext'] : '' }}";

                            var group = "{{ $section }}";
                            var nameField = "{{ $index }}";
                            var randID = "{{ $rand = 'id' . time() }}";
                            var container = [];


                            //if we are in translate page and that is for the first time, then we are resetting form

                            @if (isset($translate) && $translate && !isset($editingLang))
                                @if (is_array($item['value']))
                                    value = [];
                                @else
                                    value = '';
                                @endif
                                $('#pagename').val('');
                            @endif


                            container = $('#' + newId).closest('.group');
                            var element = getField(fieldType, group, nameField, value, resultObject, true,
                            helpText);
                            //append items

                            container.find('.list-group').append(
                                '<a href="javascript:void(0);" class="list-group-item list-group-item-action">' +
                                // $('.swal2-select option:selected').text()+
                                element +
                                '<div class="pull-right">' +
                                @if (in_array(
                                    \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                                    config('ch-pagebuilder.allowedRoles', ['Super Admin', 'Admin'])))
                                    '<i class="fa fa-arrows move"></i></span> ' +
                                @endif
                                (fieldType != 'gallery' ? '<span></span>' :
                                    '<span aria-hidden="true" class="edit-gallery" style="margin-left: 10px;" data-group="' +
                                    group + '" data-name="' + nameField + '"><i class="fa fa-edit"></i></span>'
                                    ) +
                                @if (in_array(
                                    \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                                    config('ch-pagebuilder.deleteAllowedRoles', ['Super Admin'])))
                                    '<span aria-hidden="true" class="deleteField" style="margin-left: 10px"><i class="fa fa-trash"></i></span></div></a>\n' +
                                @endif
                                '');

                            enableDragable();
                            initializeTinyMCE();
                        @endforeach
                    @endforeach
                @endif
            @endif

            dragula($('.sections').toArray(), {
                moves: function(el, container, handle) {
                    return handle.classList.contains('move-section');
                }
            });

            function enableDragable(container) {
                var containers = $('.list-group').toArray();

                if (nestedDrake) {
                    nestedDrake.destroy();
                }
                nestedDrake = dragula(containers, {
                    accepts: function(el, target, source, sibling) {
                        // accept drags only if the drop target is the same
                        // as the source container the element came from
                        return target == source;
                    },
                    moves: function(el, container, handle) {
                        return handle.classList.contains('move');
                    }
                });

                nestedDrake.on('drop', function(el) {
                    console.log("droped");
                    // let new_group = $(el).closest('.group').find('input[name="group[]"]').val();
                    // $(el).find('input').attr('name', new_group+'[]');
                });
            }

            // var pageArray =[];

            $('#addSection').on('click', function(e) {
                e.preventDefault();
                swal({
                    title: '{{ __('ch-pagebuilder::pagebuilder.createSectionTitle') }}',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: false,
                    confirmButtonText: '{{ __('ch-pagebuilder::pagebuilder.createSectionConfirm') }}',
                }).then((result) => {
                    if (result.value) {
                        var newId = "section-" + result.value.replace(/ /g, "_");
                        returnBlock(result, newId);
                    }
                });
            });


            $('body').on('click', 'button.addSection', function(e) {
                var sectionId = $(this).data('section');
                // var newArray = [];
                e.preventDefault();
                swal({
                    title: '{{ __('ch-pagebuilder::pagebuilder.createSectionTitle') }}',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: false,
                    confirmButtonText: '{{ __('ch-pagebuilder::pagebuilder.createSectionConfirm') }}',
                }).then((result) => {
                    if (result.value) {
                        var newId = sectionId + "-" + result.value;
                        returnBlock(result, newId, '#' + sectionId);
                    }
                });

                // console.log(pageArray);
            });


            $('body').on('click', '#generateForm', function(e) {
                e.preventDefault();

                $.ajax({
                    dataType: 'json',
                    cache: false,
                    url: "{{ route('admin.pagebuilder.store') }}",
                    method: "post",
                    data: $('form').serialize(),
                    success: function(html) {

                    }
                }).done(function(response) {

                    toastr.success(response.msg, window.successMsg, {
                        timeOut: 5000,
                        fadeOut: 1000,
                        progressBar: true,
                    });

                }).fail(function(error) {

                    var messages = error.responseJSON.errors ? error.responseJSON.errors : error
                        .responseJSON.msg;

                    for (message in messages) {
                        toastr.error(messages[message], window.errorMsg);
                    }

                });

            });

            $('body').on('click', '.editSection', function(e) {
                e.preventDefault();
                var thisBtn = $(this);
                let original_name = $(this).data('original-name').replace(/ /g, "_");
                let heading = $(this).closest('.group').find('.card-title').first();

                Swal({
                    title: '{{ __('ch-pagebuilder::pagebuilder.editSectionTitle') }}',
                    input: 'text',
                    inputValue: original_name,
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: false,
                    confirmButtonText: '{{ __('ch-pagebuilder::pagebuilder.saveEditSectionConfirm') }}',
                }).then((result) => {
                    var resultvalue = result.value;

                    if (resultvalue) {
                        $('body').find('input[value="' + original_name + '"]').first().val(
                            resultvalue.replace(/ /g, "_"));
                        $('body').find('input[name="group[' + original_name + '][]"]').first().attr(
                            'name', "group[" + resultvalue.replace(/ /g, "_") + '][]');
                        thisBtn.data('original-name', resultvalue.replace(/ /g, "_"));

                        //get original name field we have now
                        // var originalName = $('body').find('.dynamicfield').attr('name');
                        // var newName = originalName.replace(original_name, resultvalue.replace(/ /g, "_"));

                        // var elements = $('body').find('.dynamicfield').attr('name', newName);

                        $('body').find('.dynamicfield').each(function() {
                            var originalName = $(this).attr('name');
                            var newName = originalName.replace(original_name, resultvalue
                                .replace(/ /g, "_"));
                            var elements = $(this).attr('name', newName);
                        });


                        heading.text(resultvalue.replace(/ /g, "_"));
                    }
                });

            });


            $('body').on('click', '.addField', function(e) {
                let container = $(this).closest('.group');
                let group = container.find('input[name="group[]"]').val();
                swal({
                    title: '{{ __('ch-pagebuilder::pagebuilder.selectField') }}',
                    html: '<p style="float: left;" autofocus>Name:</p>' +
                        '<input id="swal-input1" class="swal2-input" value="' + group + '-' + $
                        .now() + '">' +
                        '<select id="swal-input2" class="swal2-input" >' +
                        '<option value="text">text</option>' +
                        '<option value="textarea">textarea</option>' +
                        '<option value="gallery">gallery</option>' +
                        '<option value="rich-editor">rich editor</option>' +
                        '<option value="button">button</option>' +
                        '<select>',
                    preConfirm: () => {
                        let fieldname = Swal.getPopup().querySelector('#swal-input1').value;
                        if (fieldname === '') {
                            Swal.showValidationMessage(`Name field empty`)
                        }
                        return {
                            fieldname: fieldname
                        }
                    },

                }).then(function(result) {

                    if (document.getElementById('swal-input2').value) {
                        //get name field : this will be the key for json entry
                        var nameField = document.getElementById('swal-input1').value.replace(/ /g,
                            "_");
                        //field type text,textarea etc
                        var fieldType = document.getElementById('swal-input2').value;
                        //get html block by selected fields
                        var element = getField(fieldType, group, nameField);
                        //append items
                        container.find('.list-group').append(
                            '<a href="javascript:void(0);" class="list-group-item list-group-item-action">' +
                            // $('.swal2-select option:selected').text()+
                            element +
                            '<div class="pull-right"> <span aria-hidden="true" class="move" style="cursor: move"><i class="fa fa-arrows move"></i></span>' +
                            (fieldType === 'gallery' &&
                                '<span aria-hidden="true" class="edit-gallery" style="margin-left: 10px;" data-group="' +
                                group + '" data-name="' + nameField +
                                '"><i class="fa fa-edit"></i></span>') +
                            '<span aria-hidden="true" class="deleteField" style="margin-left: 10px"><i class="fa fa-trash"></i></span></div></a>\n'
                        )


                        var containers = $('.list-group').toArray();
                        if (nestedDrake) {
                            nestedDrake.destroy();
                        }
                        nestedDrake = dragula(containers, {
                            accepts: function(el, target, source, sibling) {
                                // accept drags only if the drop target is the same
                                // as the source container the element came from
                                return target == source;
                            },
                            moves: function(el, container, handle) {
                                return handle.classList.contains('move');
                            }
                        });

                        nestedDrake.on('drop', function(el) {
                            console.log("droped");
                            // let new_group = $(el).closest('.group').find('input[name="group[]"]').val();
                            // $(el).find('input').attr('name', new_group+'[]');
                        });


                        //initialize tinyMCE for the rich editors dynamically
                        initializeTinyMCE();
                        $(function() {

                            var imagepicker = $('.multi-image-picker[name="' + group + '[' +
                                nameField + '][value][]"]');
                            if (imagepicker.length > 0) {
                                imagepicker.imagepicker();
                                // imagepicker.data('picker').sync_picker_with_select();
                            }
                        });
                    }
                });
            });

            $('body').on('change', '.multi-image-picker', function() {
                $('.multi-image-picker').imagepicker();
            });

            //get fields to
            function getField(fieldType, group, nameField, value, resultObject, loaded = false, helptext = '') {

                nameField = nameField.replace(/ /g, "_");
                group = group.replace(/ /g, "_");
                value = value ? value : "";
                helptext = helptext ? helptext : "";
                resultObject = resultObject ? resultObject : {
                    value: '',
                    link: '',
                    target: '_blank'
                };
                switch (fieldType) {
                    case "text":
                        var element = '<label for="first_name" class="control-label">' + nameField + '</label>' +
                            @if (Auth::user()->isSuperAdmin())
                                '<input type="text" placeholder="{{ __('ch-pagebuilder::pagebuilder.helptexts.text') }}" name="' +
                                group + '[' + nameField +
                                    '][helptext]" class="dynamicfield form-control col-md-8" value="' + helptext +
                                    '"><hr>' +
                            @elseif (Auth::user()->isAdmin())
                                '<p class="font-itali">' + helptext + '<p>' +
                            @else
                                '<p class="font-itali"><p>' +
                            @endif
                        '<input type="text"  name="' + group + '[' + nameField +
                            '][value]" class="dynamicfield form-control col-md-8" value="' + value + '">' +
                            '<input type="hidden" name="' + group + '[' + nameField +
                            '][type]" class="form-control col-md-8 dynamicfield" value="' + fieldType + '" >';



                        break;
                    case "textarea":

                        var element = '<label for="first_name" class="control-label">' + nameField + '</label>' +
                            @if (Auth::user()->isSuperAdmin())
                                '<input type="text" placeholder="{{ __('ch-pagebuilder::pagebuilder.helptexts.textarea') }}" name="' +
                                group + '[' + nameField +
                                    '][helptext]" class="dynamicfield form-control col-md-8" value="' + helptext +
                                    '"><hr>' +
                            @elseif (Auth::user()->isAdmin())
                                '<p class="font-itali">' + helptext + '<p>' +
                            @else
                                '<p class="font-itali"><p>' +
                            @endif
                        '<textarea name="' + group + '[' + nameField +
                            '][value]" class="dynamicfield form-control col-md-8">' + value + '</textarea>' +
                            '<input type="hidden" name="' + group + '[' + nameField +
                            '][type]" class="form-control col-md-8 dynamicfield" value="' + fieldType + '" >';
                        break;
                    case "gallery":
                        valueArray = value;
                        // valueArray = [value.replace('[',"").replace(']',"")];

                        const modal = $("#galleryModal");
                        const images =
                            ' <select name="' + group + '[' + nameField +
                            '][value][]" class="multi-image-picker dynamicfield" multiple="multiple" data-target="' +
                            group + '[' + nameField + ']">' +

                            @foreach ($galleryImages as $image)
                                '<option ' + $.inArray("{{ Storage::url($image->path) }}", valueArray).toString()
                                    .replace(/^[0-9]+(\\.[0-9]{1,2})?$/g, 'selected') +
                                    ' data-img-src="{{ Gallery::thumbnail($image->path, 200) }}" data-img-title="{{ $image->title }}" value="{{ Storage::url($image->path) }}">Cute Kitten 1</option>' +
                            @endforeach

                        '</select>';

                        const header = '<label for="first_name" class="control-label">' + nameField + '</label>' +
                            @if (Auth::user()->isSuperAdmin())
                                '<input type="text" placeholder="{{ __('ch-pagebuilder::pagebuilder.helptexts.gallery') }}" name="' +
                                group + '[' + nameField +
                                    '][helptext]" class="dynamicfield form-control col-md-8" value="' + helptext +
                                    '"><hr>' +
                            @elseif (Auth::user()->isAdmin())
                                '<p class="font-itali">' + helptext + '<p>' +
                            @else
                                '<p class="font-itali"><p>' +
                            @endif
                        '<input type="hidden" name="' + group + '[' + nameField +
                            '][type]" class="form-control col-md-8 dynamicfield" value="' + fieldType + '" >';
                        if (!loaded) {
                            modal.find('.modal-body').html(images);
                            modal.modal('show');
                            element = header;
                        } else {
                            let imgs = '';
                            let imgs_show = '';
                            valueArray.forEach((image, index) => {
                                if (image) {
                                    imgs +=
                                        `<input name="${group}[${nameField}][value][${index}]" class="old_val" type="hidden" value="${image}">`;
                                    imgs_show += `<img src="${image}" class="gallery_image">`;
                                }
                            });
                            if (imgs === '') {
                                imgs += `<input name="${group}[${nameField }][value][0]" type="hidden" value="">`;
                            }
                            element = header + imgs + imgs_show;
                        }

                        break;
                    case "rich-editor":
                        var element = '<label for="first_name" class="control-label">' + nameField + '</label>' +
                            @if (Auth::user()->isSuperAdmin())
                                '<input type="text" placeholder="{{ __('ch-pagebuilder::pagebuilder.helptexts.richeditor') }}" name="' +
                                group + '[' + nameField +
                                    '][helptext]" class="dynamicfield form-control col-md-8" value="' + helptext +
                                    '"><hr>' +
                            @elseif (Auth::user()->isAdmin())
                                '<p class="font-itali">' + helptext + '<p>' +
                            @else
                                '<p class="font-itali"><p>' +
                            @endif
                        '<textarea name="' + group + '[' + nameField +
                            '][value]" class="dynamicfield form-control col-md-8 summernote-body">' + value +
                            '</textarea>' +
                            '<input type="hidden" name="' + group + '[' + nameField +
                            '][type]" class="form-control col-md-8 dynamicfield" value="' + fieldType + '" >';
                        break;
                    case "button":
                        console.log(resultObject);
                        var target = (resultObject.target) ? resultObject.target : "_blank";
                        var link = (resultObject.link) ? resultObject.link : "";
                        var btnNme = (resultObject.value) ? resultObject.value : "";
                        var element = '<label for="first_name" class="control-label">' + nameField + '</label>' +
                            @if (Auth::user()->isSuperAdmin())
                                '<input type="text" placeholder="{{ __('ch-pagebuilder::pagebuilder.helptexts.button') }}" name="' +
                                group + '[' + nameField +
                                    '][helptext]" class="dynamicfield form-control col-md-8" value="' + helptext +
                                    '"><hr>' +
                            @elseif (Auth::user()->isAdmin())
                                '<p class="font-itali">' + helptext + '<p>' +
                            @else
                                '<p class="font-itali"><p>' +
                            @endif
                        '<label for="first_name" class="control-label">{{ __('ch-pagebuilder::pagebuilder.labels.buttontext') }}  </label>' +
                        '<input type="text" name="' + group + '[' + nameField +
                            '][value]" class="dynamicfield form-control col-md-8" value="' + btnNme + '">' +
                            '<label for="first_name" class="control-label">{{ __('ch-pagebuilder::pagebuilder.labels.buttonlink') }}  </label>' +
                            '<input type="text" name="' + group + '[' + nameField +
                            '][link]" class="dynamicfield form-control col-md-8" value="' + link + '">' +
                            '<label for="first_name" class="control-label">{{ __('ch-pagebuilder::pagebuilder.labels.buttontarget') }}  </label>' +
                            ' <select name="' + group + '[' + nameField +
                            '][target]" class="dynamicfield form-control col-md-8" >' +
                            '<option ' + target.toString().replace('_blank', "selected") +
                            ' value="_blank">_blank</option>' +
                            '<option ' + target.toString().replace("_self", "selected") +
                            ' value="_self">_self</option>' +
                            '</select>' +
                            '<input type="hidden" name="' + group + '[' + nameField +
                            '][type]" class="form-control col-md-8 dynamicfield" value="' + fieldType + '" >';
                        break;
                }
                return element;

            }

            $('#galleryModal').on('hide.bs.modal', function(event) {
                var modal = $(this)
                const images = modal.find('.modal-body select').val();
                const target = modal.find('.modal-body select').data("target");
                let imgs = '';
                let imgs_show = '';
                images.forEach((image, index) => {
                    imgs +=
                        `<input name="${target}[value][${index}]" type="hidden" class="gallery_image_value" value="${image}">`;
                    imgs_show += `<img src="${image}" class="gallery_image">`;
                });
                if (imgs === '') {
                    imgs +=
                        `<input name="${target}[value][0]" type="hidden" class="gallery_image_value" value="">`;
                }
                if (!appendGallery) {
                    // $(`input[name="${target}[type]"]`).find('.gallery_image_value').attr('disabled',true);
                    $(`input[name="${target}[type]"]`).parent().find('.old_val').remove();
                    $(`input[name="${target}[type]"]`).parent().find('.gallery_image').remove();
                }
                $(`input[name="${target}[type]"]`).parent().append(imgs);
                $(`input[name="${target}[type]"]`).parent().append(imgs_show);
            });

            $('#galleryModal').on('show.bs.modal', function(event) {
                $('.multi-image-picker').imagepicker();
            });

            $('body').on('click', '.edit-gallery', function(e) {
                const name = $(this).data('name');
                const group = $(this).data('group');
                // asd[asd-1587632868778][value]
                const values = [];
                const images = $(`input[name="${group}[${name}][type]"]`).parent().find('.gallery_image');
                images.each((index, image) => {
                    values.push($(image).attr('src'));
                });
                appendGallery = false;
                getField('gallery', group, name, values, false, false, '', true);
            });

            //initialize tinymce
            function initializeTinyMCE() {
                $('.summernote-body').summernote({
                    height: 350,
                    toolbar: [
                        ['font', ['bold', 'underline', 'clear']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['insert', ['link', 'picture', 'video']],
                        ['view', ['fullscreen', 'help']],
                    ],
                });
                // tinymce.init({
                //     selector: '.summernote-body', setup: function (editor) {
                //         editor.on('change', function () {
                //             tinymce.triggerSave();
                //         });
                //         editor.on('init', function (args) {
                //             editor = args.target;
                //
                //             editor.on('NodeChange', function (e) {
                //                 // if (e && e.element.nodeName.toLowerCase() == 'img') {
                //                 //     tinyMCE.DOM.setAttribs(e.element, {'width': '100%', 'height': null});
                //                 // }
                //             });
                //         });
                //     },
                // });
            }

            //delete field
            $('body').on('click', '.deleteField', function(e) {
                $(this).closest('.list-group-item').remove();
            });

            //delete section
            $('body').on('click', '.deleteSection', function(e) {

                Swal({
                    title: 'Are you sure?',
                    text: "Really delete this section?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $(this).closest('.group').remove();
                    }
                });
            });

            //return sections
            function returnBlock(result, newId, parent) {
                var parent = parent ? parent : '.sections';

                var resultValue = result.value.replace(/ /g, "_");

                return $(parent).append('<div class="card cardWithShadow group draggable-section">\n' +

                    '<div class="card-header section" id="' + newId + '">\n' +
                    '<input type="hidden" class="indexelement" data-index="index[]" name="group[]" value="' +
                    resultValue + '">\n' +
                    @if (in_array(
                        \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                        config('ch-pagebuilder.allowedRoles', ['Super Admin', 'Admin'])))
                        '<i class="fa fa-arrows move-section"></i><hr>' +
                    @endif
                    '<h4 class="card-title">' + resultValue + '</h4>\n' +
                    '<a class="heading-elements-toggle"><i class="fa fa-ellipsis font-medium-3"></i></a>\n' +
                    '        <div class="heading-elements">\n' +
                    '<ul class="list-inline mb-0">\n' +
                    @if (in_array(
                        \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                        config('ch-pagebuilder.allowedRoles', ['Super Admin', 'Admin'])))
                        '<li><button type="button" class="page-link btn-sm addField" data-group="' + resultValue
                            +
                            '" aria-label="Previous">\n' +
                            '<span aria-hidden="true"><i class="fa fa-plus-circle"></i> {{ __('ch-pagebuilder::pagebuilder.insertField') }}</span>\n' +

                            '</button></li>\n' +
                            '<li><button type="button" class="page-link btn-sm editSection" data-original-name="' +
                            resultValue + '" aria-label="Previous">\n' +
                            '<span aria-hidden="true"><i class="fa fa-pencil"></i> {{ __('ch-pagebuilder::pagebuilder.edit') }}</span>\n' +
                            '</button></li>' +

                            @if (in_array(
                                \Illuminate\Support\Facades\Auth::user()->roles->first()->name,
                                config('ch-pagebuilder.deleteAllowedRoles', ['Super Admin'])))

                                '<li style="margin-left: 3px"><button type="button" class="page-link btn-sm deleteSection" aria-label="Previous">\n' +
                                '<span aria-hidden="true"><i class="fa fa-trash"></i> {{ __('ch-pagebuilder::pagebuilder.delete') }}</span>\n' +
                                '</button></li>' +
                            @endif
                    @endif

                    '</ul>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '<div class="card-content collapse show">\n' +
                    '<div class="card-body">\n' +
                    '<div class="list-group">\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</div>');
            }
        });
    </script>
@endsection
