<?php
return [
    "createSectionTitle" => "Create section",
    "createSectionConfirm" => "Confirm",
    "editSectionTitle" => "Edit Section",
    "saveEditSectionConfirm" => "Confirm",
    "selectField" => "Select field",
    "insertField" => "Insert new field",
    "addSection" => "Add Section",
    "edit" => "Edit",
    "delete" => "Delete",
    "createSection" => "Create new section",
    "geneateform" => "Save",
    "pagename" => "Page name",
    "galleryModal" => [
        "heading" => 'Choose gallery images',
        "confirmButton" => 'Insert images'
    ],

    "Id" => "ID",
    "Identifier" => "Identifier",
    "Name" => "Page Name",
    "Design new page" => "New Page Structure",

    "Update" => "Update",
    "Delete" => "Delete",
    "Pagebuilder" => "Pagebuilder",

    "The Page Name field is required." => "The Page Name field is required.",
    "Cannot Submit empty page structure" => "Cannot Submit empty page structure",
    "Error Occured" => "Error Occured",
    "Page Designed Successfully" => "Page Designed Successfully",

    "help" => [
        "heading" => "How To",
        "step1" => "First you have to create a new section. to do that click on the button below. you can create sections as much as you need.",
        "step2" => "Then you can create new fields by clicking on insert new field button.",
        "step3" => "You can select any number of fields from (text,textarea,WYSIWYG editor,gallery)",
        "step4" => "Make sure to have meaningful names when you are creating sections and fields. as those will be the keys in the json you get finally.",
        "step5" => "Once you are completed with filling data, you can build your page.",
    ],

    "admin.pagebuilder.index" => "Page Structures",
    "admin.pagebuilder.create" => "Create new Structure",
    "admin.pagebuilder.edit" => "Edit Structure",

    "labels" => [
        "buttontext" => "Button text",
        "buttonlink" => "Button link",
        "buttontarget" => "Button target",
    ],
    'helptexts' => [
        "text" => "help text for fields text",
        "textarea" => "help text for fields textarea",
        "gallery" => "help text for fields gallery",
        "richeditor" => "help text for fields richeditor",
        "button" => "help text for fields button",
    ],
    "lanugage_not_available"=>"Language is not available",
    'Translate'=>"Translate",
    'DeleteTranslation'=>"Delete Translation",
    'language'=>"Translating Language",
];