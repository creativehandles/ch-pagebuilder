<?php
namespace Creativehandles\ChPagebuilder\Repositories;

use App\Repositories\BaseEloquentRepository;
use App\User;
use Creativehandles\ChPagebuilder\Http\Resources\PagebuilderResource;
use Creativehandles\ChPagebuilder\Models\PageBuilder;

class PageBuilderRepository extends BaseEloquentRepository
{

    public function getModel()
    {
        return new PageBuilder();
    }

    public function getResource()
    {
        return PagebuilderResource::class;
    }

    public function getResourceCollection(){
        return null;
    }

}
