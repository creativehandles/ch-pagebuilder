<?php

namespace Creativehandles\ChPagebuilder\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class BuildPageBuilderPackageCommand extends Command
{
    protected $signature = 'creativehandles:build-pagebuilder-plugin';

    protected $description = 'Build all prerequisites to start the plugin';

    public function handle()
    {
        $plugin = 'PageBuilder';

        $this->info('Publishing vendor directories');
        $this->callSilent('vendor:publish', ['--provider' => 'Creativehandles\ChPagebuilder\ChPagebuilderServiceProvider']);

        //seed active plugins table
        if (! DB::table('active_plugins')->where('plugin', $plugin)->first()) {
            DB::table('active_plugins')->insert([
                'plugin'=>$plugin,
            ]);
        }

        $this->info('Good to go!!');
    }
}
