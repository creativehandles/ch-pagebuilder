<?php

namespace  Creativehandles\ChPagebuilder\Http\Resources;

use App\Http\Resources\CoreJsonResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PagebuilderResource extends CoreJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $resource=[
          'id'=>$this->id,
          'identifier'=>$this->page_identifier,
          'name'=>$this->page_name,
          'content'=>$this->content
        ];

        return $resource;
    }


}
