<?php

namespace Creativehandles\ChPagebuilder\Http\Controllers\PluginsControllers;


use Creativehandles\ChGallery\Plugins\Gallery\Gallery;
use Creativehandles\ChPagebuilder\Models\PageBuilder;
use Creativehandles\ChPagebuilder\Repositories\PageBuilderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class PageBuilderController
{

    /**
     * @var PageBuilderRepository
     */
    private $builderRepository;

    public function __construct(PageBuilderRepository $builderRepository)
    {
        $this->builderRepository = $builderRepository;
    }

    public function index()
    {
        return view('ch-pagebuilder::PageBuilder.index');
    }

    public function create()
    {
        $galleryImages = Gallery::getImageWithData();
        return view('ch-pagebuilder::PageBuilder.create')->with(['galleryImages' => $galleryImages]);
    }

    public function edit(Request $request, $id)
    {

        //get language from the request
        $requestedLang = $request->get('lang');
        //set locale for the moment
        if($requestedLang)
            app()->setLocale($requestedLang);

        $model = $this->builderRepository->byId($id);

        //get gallery images
        $galleryImages = Gallery::getImageWithData();

        //check translate flag
        $translate = (bool) $request->get('translate', 0);


        //get all available locales
        $availableLangs = array_keys(config('laravellocalization.supportedLocales'));


        return view('ch-pagebuilder::PageBuilder.create')->with([
            'galleryImages' => $galleryImages, 'model' => $model, 'translate' => $translate,
            'availableLangs' => $availableLangs,'requestedLang'=>$requestedLang
        ]);
    }

    public function store(Request $request)
    {
        $all = $request->except(['_token', 'method', 'uri', 'ip']);

        //get language from the request
        $requestedLang = $request->get('language', app()->getLocale());

        //set locale for the moment
        app()->setLocale($requestedLang);

        $rules = [
            'pagename' => 'required',
            'page_identifier' => 'required',
            'group' => 'required',
        ];

        $messages = [
            'pagename.required' => __('ch-pagebuilder::pagebuilder.The Page Name field is required.'),
            'group.required' => __('ch-pagebuilder::pagebuilder.Cannot Submit empty page structure'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['msg' => $validator->errors()->toArray()], 400);
        }

        $data = [
            'page_name' => $request->get('pagename'),
            'page_identifier' => $request->get('page_identifier'),
            'sections' => json_encode($request->get('group', [])),
        ];


        //get all groups
        $groups = $request->get('group', []);

        $formattedArray = [];

        foreach ($groups as $group) {
            //get elements inside group
            $item = $request->get($group);

            if ($item) {
                $formattedArray[$group] = $item;
            }

        }

        $data['content'] = json_encode($formattedArray);

        try {
            $page = $this->builderRepository->updateOrCreate($data, 'page_identifier');
        } catch (\Exception $e) {
            return response()->json(['msg' => __("ch-pagebuilder::pagebuilder.Error Occured")], 400);
        }

        return response()->json([
            'msg' => __("ch-pagebuilder::pagebuilder.Page Designed Successfully"), 'data' => $page
        ], 200);

    }

    public function pageBuilderGrid()
    {
        $searchColumns = ['pagebuilder.id', 'pagebuilder.page_identifier','pagebuilder.page_name'];
        $orderColumns = ['1' => 'pagebuilder.id', '2' => 'pagebuilder.page_identifier'];
        $with = [];
        $data = $this->builderRepository->getDataforDataTables($searchColumns, $orderColumns, '2', $with);

        $availableTranslations = "<b>Translations Available<b> : <br>";
        $availableTranslations = "";
        $translateBtns = '';
        $forgetBtns = '';

        $data['data'] = array_map(function ($item) use ($availableTranslations, $translateBtns, $forgetBtns) {


            foreach (Arr::get($item,'page_name',[]) as $key => $name) {
                $availableTranslations .= "$key - $name "."<br><hr>";

                $translateBtns .= '<a href="'.route('admin.pagebuilder.edit', [
                        'pagebuilder' => $item['id'], 'lang' => $key
                    ]).'" class="btn btn-info btn-sm">'.__("ch-pagebuilder::pagebuilder.Update").'</a><br><hr>';
                if(count($item['page_name']) >1 ) {
                    $id = $item['id'];
                    $forgetBtns .= '<a href="#" data-id="'.$id.'" data-lang="'.$key.'"  class="btn btn-danger btn-sm deleteTranslation">'.__("ch-pagebuilder::pagebuilder.DeleteTranslation").'</a><br><hr>';
                }
            }

            $return = [
                $item['id'],
                $item['page_identifier'],
                $availableTranslations,
                $translateBtns,
                $forgetBtns
            ];

            return $return;
        }, $data['data']);

        return json_encode($data);
    }

    public function destroy(Request $request, $id)
    {

        $pageBuilder = PageBuilder::findOrFail($id);

        if ($deleteLang = $request->get('lang')) {
            $pageBuilder->forgetAllTranslations($deleteLang);
            $pageBuilder->save();
            return response()->json(['msg' => "Deleted Translation :".$deleteLang], 200);
        }

        $pageBuilder->delete();
        return response()->json(['msg' => "Deleted"], 200);

    }

}
