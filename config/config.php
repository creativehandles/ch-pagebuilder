<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'allowedRoles' => explode(",", env('PAGEBUILDER_ALLOWED_ROLES',"Super Admin,Admin")),
    'deleteAllowedRoles' => explode(",", env('PAGEBUILDER_DELETE_ALLOWED_ROLES',"Super Admin")),
];
