# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/ch-pagebuilder.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-pagebuilder)
[![Build Status](https://img.shields.io/travis/creativehandles/ch-pagebuilder/master.svg?style=flat-square)](https://travis-ci.org/creativehandles/ch-pagebuilder)
[![Quality Score](https://img.shields.io/scrutinizer/g/creativehandles/ch-pagebuilder.svg?style=flat-square)](https://scrutinizer-ci.com/g/creativehandles/ch-pagebuilder)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/ch-pagebuilder.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-pagebuilder)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/ch-pagebuilder
```

## Usage
Publish required files (migrations,routes,views etc) to CORE CMS
```php
php artisan vendor:publish --provider="Creativehandles\ChPagebuilder\ChPagebuilderServiceProvider"
```

Set following .env keys
```
//only these user roles can add/re-structure
PAGEBUILDER_ALLOWED_ROLES="Super Admin,Admin"
//can delete only these user roles
PAGEBUILDER_DELETE_ALLOWED_ROLES="Super Admin"
//other user roles can only edit the content.
```

Migrate database tables
```
php artisan migrate
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

- [Deemantha Kasun](https://github.com/creativehandles)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
